﻿using anca.engine.commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;

namespace anca.engine.interfaces
{
	interface IPromptingSentenceBuilder
	{
		
		Dictionary<String, AncaCommandBase> GetLoadedPrompts();
		void AddCommand(AncaCommandBase cmd, string choice);
		string GetLoadedPromptsString();
		Grammar getGrammar();
	}
}
