﻿using anca.engine.interfaces;
using anca.engine.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anca.engine.builders
{
	class GreetingBuilder : ISentenceBuilder
	{		
		private readonly string _name;
		private readonly string _timeOfDay;
		private readonly string _greetingForm;

		/// <summary>
		/// TODO: replace hard coded strings.
		/// </summary>
		public GreetingBuilder()
		{
			_greetingForm = determineGreeting();			
			_name = "Captain";
			_timeOfDay = DateTime.Today.DayOfWeek + " " + DateTime.Now.ToString();
		}

		private string determineGreeting()
		{
			TimeUtils.TimeOfDay timeOfDay = TimeUtils.CurrentTimeOfDay();
			switch (timeOfDay)
			{
				case TimeUtils.TimeOfDay.Morning:
					return "Good Morning";
					
				case TimeUtils.TimeOfDay.Noon:
					return "It is good to see you";
					
				case TimeUtils.TimeOfDay.Afternoon:
					return "Good afternoon";
					
				case TimeUtils.TimeOfDay.Evening:
					return "It is a wonderful evening";
					
				case TimeUtils.TimeOfDay.Night:
					return "It is getting very late, you should prepare for bed, ";
					
				case TimeUtils.TimeOfDay.YouShouldSleep:
					return "You really should be in bed, ";
					
				case TimeUtils.TimeOfDay.WhyAreYouAwakeThisEarly:
					return "You really should prepare for bed, ";
					
				default:
					break;
			}
			return "I feel a little funny today...";

		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>String Anca can pronounce</returns>
		public string BuildString()
		{
		
			return _greetingForm + "  " + _name + ". " + " Today is: " + _timeOfDay + ".";
		}
	}
}
