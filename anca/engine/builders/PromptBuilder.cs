﻿using anca.engine.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using anca.engine.commands;
using System.Speech.Recognition;

namespace anca.engine.builders
{
	class PromptBuilder : ISentenceBuilder, IPromptingSentenceBuilder
	{

		Dictionary<String, AncaCommandBase> storedCommands;

		/// <summary>
		/// 
		/// </summary>
		public PromptBuilder()
		{
			storedCommands = new Dictionary<string, AncaCommandBase>();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="choice"></param>
		public void AddCommand(AncaCommandBase cmd, string choice)
		{
			storedCommands.Add(choice, cmd);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string BuildString()
		{
			int count = 1;
			string ret = "";
			foreach (var cmd in storedCommands)
			{
				ret += count++.ToString() + ": " + cmd.Key + "\n";
			}
			return "\n" + ret;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Grammar getGrammar()
		{
			var choices = new Choices();
			string[] temp = new string[storedCommands.Count];

			for (int i = 0; i < storedCommands.Count; i++)
			{
				temp[i] = storedCommands.Keys.ElementAt(i);
				Console.WriteLine(temp[i].ToString());
			}

			choices.Add(temp);
			var grammarBuilder = new GrammarBuilder();
			grammarBuilder.Append(choices);
			return new Grammar(grammarBuilder);
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, AncaCommandBase> GetLoadedPrompts()
		{
			return storedCommands;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetLoadedPromptsString()
		{
			return BuildString();
		}
	}
}
