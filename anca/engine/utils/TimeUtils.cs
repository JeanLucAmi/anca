﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anca.engine.utils
{

	public static class TimeUtils
	{
		public static TimeSpan MORNING_START = new TimeSpan(5, 0, 0);
		public static TimeSpan NOON_START = new TimeSpan(12, 0, 0);
		public static TimeSpan AFTERNOON_START = new TimeSpan(15, 0, 0);
		public static TimeSpan EVENING_START = new TimeSpan(18, 0, 0);
		public static TimeSpan NIGHT_START = new TimeSpan(22, 0, 0);
		public static TimeSpan YOU_SHOULD_SLEEP_START = new TimeSpan(24, 0, 0);
		public static TimeSpan WHY_ARE_YOU_AWAKE_THIS_EARLY_START = new TimeSpan(3, 0, 0);

		public enum TimeOfDay
		{
			Morning = 0,
			Noon = 1,
			Afternoon = 2,
			Evening = 3,
			Night = 4,
			YouShouldSleep = 5,
			WhyAreYouAwakeThisEarly = 6
		}


		public static TimeOfDay CurrentTimeOfDay()
		{
			TimeSpan now = (TimeSpan)DateTime.Now.TimeOfDay;
			if (now > MORNING_START && now < NOON_START)
			{
				return TimeOfDay.Morning;
			}
			if (now > NOON_START && now < AFTERNOON_START)
			{
				return TimeOfDay.Noon;
			}
			if (now > AFTERNOON_START&& now < EVENING_START)
			{
				return TimeOfDay.Afternoon;
			}
			if (now > EVENING_START && now < NIGHT_START)
			{
				return TimeOfDay.Evening;
			}
			if (now > NIGHT_START && now < YOU_SHOULD_SLEEP_START)
			{
				return TimeOfDay.Night;
			}
			if (now > YOU_SHOULD_SLEEP_START && now < WHY_ARE_YOU_AWAKE_THIS_EARLY_START)
			{
				return TimeOfDay.YouShouldSleep;
			}
			if (now > WHY_ARE_YOU_AWAKE_THIS_EARLY_START && now < MORNING_START)
			{
				return TimeOfDay.WhyAreYouAwakeThisEarly;
			}
			return TimeOfDay.Morning;

		}
	}
}
