﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.Collections.ObjectModel;
using anca.Properties;
using anca.engine.commands;

namespace anca.engine
{
	/// <summary>
	/// The Anca Object
	/// </summary>
	public class Anca
	{
		private readonly SpeechRecognitionEngine _ear = new SpeechRecognitionEngine((new System.Globalization.CultureInfo("en-US")));
		private readonly SpeechSynthesizer _voice = new SpeechSynthesizer();
		private Grammar _currentGrammar;
		private Dictionary<string, AncaCommandBase> _availableCommands;

		private bool _currentInputMatched = false;

		public Action<AncaCommandBase> CommandCompleted;


		// Constructor
		public Anca()
		{
			InitializeVoiceSynthesis();
			_ear.SetInputToDefaultAudioDevice();
			_ear.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(ear_DigestSpeech);
			_ear.SpeechDetected += new EventHandler<SpeechDetectedEventArgs>(OnSpeechDetected);
			_ear.SpeechRecognitionRejected += new EventHandler<SpeechRecognitionRejectedEventArgs>(OnRejected);
			_ear.RecognizeCompleted += new EventHandler<RecognizeCompletedEventArgs>(OnAsyncComplete);

		}

		public void RegisterCommandCompleted(Action<AncaCommandBase> callback)
		{
			CommandCompleted += callback;
		}


		public void SetAvailableCommands(Dictionary<string,AncaCommandBase> pCommands)
		{
			_availableCommands = pCommands;
		}

		/// <summary>
		/// Initializes the voice synthesis. Stolen from Scott Lilly
		/// </summary>
		private void InitializeVoiceSynthesis()
		{
			ReadOnlyCollection<InstalledVoice> voices = _voice.GetInstalledVoices(); // Get all voices installed in system

			// check for settings and act accordingly
			if (!string.IsNullOrWhiteSpace(Settings.Default.VoiceName) && voices.Any(v => v.VoiceInfo.Name == Settings.Default.VoiceName)) 
			{
				_voice.SelectVoice(Settings.Default.VoiceName);
				
			}
			else
			{
				if (Settings.Default.VoiceGender.Equals("Male",
													 StringComparison.CurrentCultureIgnoreCase))
				{
					_voice.SelectVoiceByHints(VoiceGender.Male);
				}
				else if (Settings.Default.VoiceGender.Equals("Female",
															StringComparison.CurrentCultureIgnoreCase))
				{
					_voice.SelectVoiceByHints(VoiceGender.Female);
				}
				else
				{
					_voice.SelectVoiceByHints(VoiceGender.Neutral);
				}
			}

			_voice.Rate = Settings.Default.VoiceRate;
			_voice.Volume = Settings.Default.VoiceVolume;
		}

		public void Speak(string pMessage)
		{
			_voice.SpeakAsync(pMessage);
		}

		public void SpeakAndBlock(string pMessage)
		{
			_voice.Speak(pMessage);
		}
	

		void Execute(AncaCommandBase cmd)
		{
			cmd.ExecuteCommand();
		}

		public void listen()
		{
			Console.WriteLine("Listening");
			_ear.RecognizeAsync();
		}


		void ear_DigestSpeech(object sender, SpeechRecognizedEventArgs e)
		{
			Console.WriteLine("Speech recognized " + e.Result.Text);
			Execute(_availableCommands[e.Result.Text]);
			_currentInputMatched = true;

		}

		void OnSpeechDetected(object sender, SpeechDetectedEventArgs e)
		{
			Console.WriteLine("Speech detected "+ e.AudioPosition);
		}

		void OnRejected(object sender, SpeechRecognitionRejectedEventArgs e)
		{
			Console.WriteLine("Rejected");
			
		}

		void OnAsyncComplete(object sender, RecognizeCompletedEventArgs e)
		{
			if (_currentInputMatched)
			{
				SpeakAndBlock("You choose: " + e.Result.Text);
				_currentInputMatched = false;

				CommandCompleted(_availableCommands[e.Result.Text]);
				return;
			}

			SpeakAndBlock("i did not understand...");
			
			Console.WriteLine("Rejected ");
			listen();
		}


		public void loadGrammar(Grammar pGrammer)
		{
			_currentGrammar = pGrammer;
			_ear.LoadGrammar(_currentGrammar);
		}
	}
}
