﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;

namespace anca.engine.commands
{
	// stolen from some stackoverflow shit...added capability to write random prefix to filename
	public class AncaScreenshotCommand : AncaCommandBase
	{
		public Random r = new Random(666);
		public override void ExecuteCommand()
		{
			var bmpScreenshot = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
							   Screen.PrimaryScreen.Bounds.Height,
							   PixelFormat.Format32bppArgb);

			// Create a graphics object from the bitmap.
			var gfxScreenshot = Graphics.FromImage(bmpScreenshot);

			// Take the screenshot from the upper left corner to the right bottom corner.
			gfxScreenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
										Screen.PrimaryScreen.Bounds.Y,
										0,
										0,
										Screen.PrimaryScreen.Bounds.Size,
										CopyPixelOperation.SourceCopy);

			// get users images directory
			var path = @"%USERPROFILE\Pictures\";
			var savepath = Environment.ExpandEnvironmentVariables(path);
			var randomSuffix = "";
			// append random suffix
			
			
			randomSuffix += r.Next().ToString();
			
			// Save the screenshot to the specified path that the user has chosen.
			bmpScreenshot.Save("Screenshot"+ randomSuffix +".png", ImageFormat.Png);
		}
	}
}
