﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using anca.engine;
using anca.engine.interfaces;
using anca.engine.builders;
using anca.engine.commands;


namespace anca
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		Anca anca = new Anca();

		public MainWindow()
		{
			InitializeComponent();
			ContentRendered += new EventHandler(OnContentRendered);
			anca.RegisterCommandCompleted(OnCommandExecuted);

		}

		void OnCommandExecuted(AncaCommandBase cmd)
		{
			Console.WriteLine("Command Executed " +cmd.ToString());
			anca.listen();
		}

		void OnContentRendered(object sender, EventArgs e)
		{
			InitiateWelcomeDialog();
		}
		public void InitiateWelcomeDialog()
		{
			var greeting = new GreetingBuilder().BuildString();
			anca.Speak(greeting);

			var welcomePrompt = new PromptBuilder();
			welcomePrompt.AddCommand(new AncaCommandBase(), "I want to fuzz around a little.");
			welcomePrompt.AddCommand(new AncaCommandBase(), "I would like to surf the web.");
			welcomePrompt.AddCommand(new AncaCommandBase(), "I need to get some work done.");
			welcomePrompt.AddCommand(new AncaScreenshotCommand(), "Computer, take a screenshot");
			welcomePrompt.AddCommand(new FuckYouCommand(anca), "Fuck you!");

			string prompt = welcomePrompt.GetLoadedPromptsString();
			tbOutput.Text = greeting + "\n" + prompt;
			anca.SetAvailableCommands(welcomePrompt.GetLoadedPrompts());
			anca.loadGrammar(welcomePrompt.getGrammar());
			anca.SpeakAndBlock("what would you like to do captain");
			anca.listen();

		}


	}
}
